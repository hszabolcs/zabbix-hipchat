#!/usr/bin/env python
import json
import sys
from urllib2 import Request, urlopen

V2TOKENX =  sys.argv[1]
V2TOKEN = '[token]'
ROOMID = [roomid]

# API V2, send message to room:
url = 'https://api.hipchat.com/v2/room/%d/notification' % ROOMID
message = sys.argv[2]
message2= sys.argv[3]
headers = {
    "content-type": "application/json",
    "authorization": "Bearer %s" % V2TOKEN}
datastr = json.dumps({

    'message': message + message2,
    'color': 'green',
    'message_format': 'html',
    'notify': True})
request = Request(url, headers=headers, data=datastr)
uo = urlopen(request)
rawresponse = ''.join(uo)
uo.close()
assert uo.code == 204
